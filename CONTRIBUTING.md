# Contribute

## Add new Cards

Add a new entry in the property `Tasks` in the file `model/TaskModel.json`.

```json
{
    "id": "5.o.1",
        "category": "five_off",
            "title": "The title",
                "description": "Text <br> More text"
                }
                ```

                Description allows html tags. A list of valid tags can be found in the [UI5 API](https://sapui5.hana.ondemand.com/#/api/sap.m.FormattedText%23methods/getHtmlText)

                `category` has to match any `id` of one of the categories (defined in the same file). 
