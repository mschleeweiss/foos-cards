# Foos cards

Is a simple web app to train certain aspects of your foos game. 

Before a training game each player draws at least one card. Throughout the game you will do whatever the card says, e.g. only play reverse pass options when passing from the five bar to the three bar. For maximum training effect try to guess what your opponents cards were and discuss what worked well or did not work well.

Some of the cards only make sense when training for singles, e.g. passing from two to five. But other than that you can use this app in double training as well.

## Start app

The app is hosted directly on gitlab.com and can be accessed with [https://mschleeweiss.gitlab.io/foos-cards/](https://mschleeweiss.gitlab.io/foos-cards/)

## Local development

### Prerequisites

Install [Node.js®](https://nodejs.org/en/) and optionally this [super fast package manager](https://pnpm.js.org/en/).

### Install dependencies

    > npm install

or
    > pnpm install


### Start application
After successful setup, the web application can be started via command line with

    > npm start

or

    > pnpm start

The `start` script configures the necessary proxies, starts a web server that provides the files and monitors source code changes in the `webapp` folder.
When a change is made, the browser is automatically updated. A change to a JavaScript file is also tested with `ESLint`.

### Contribute

#### Add new Cards

Add a new entry in the property `Tasks` in the file `model/TaskModel.json`.

```json
{
    "id": "5.o.1",
    "category": "five_off",
    "title": "The title",
    "description": "Text <br> More text"
}
```

Description allows html tags. A list of valid tags can be found in the [UI5 API](https://sapui5.hana.ondemand.com/#/api/sap.m.FormattedText%23methods/getHtmlText)

`category` has to match any `id` of one of the categories (defined in the same file).