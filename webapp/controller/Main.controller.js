sap.ui.define([
    "./BaseController",
    "sap/m/MessageToast"
], function (BaseController, MessageToast) {

    const VIEW_MODEL = "view";

    return BaseController.extend("de.fooszination.cards.controller.Main", {

        onInit: function () {
            BaseController.prototype.onInit.apply(this, arguments);
            this._initRouter();
        },

        onSelectTab: function (oEvent) {
            const sKey = oEvent.getParameter("selectedKey");
            this.getRouter().navTo(sKey, null, true);
        },

        _initRouter: function () {
            this.getRouter().attachRoutePatternMatched(this._onRoutePatternMatched, this);
        },

        _onRoutePatternMatched: function (oEvent) {
            const sKey = oEvent.getParameter("name");
            if (sKey === "main") {
                this.getRouter().navTo("mytasks", null, true);
                return;
            }
            this.getModel(VIEW_MODEL).setProperty("/selectedTab", sKey);
        }

    });
}
);
