sap.ui.define([
    "./BaseController"
], function (BaseController) {

    return BaseController.extend("de.fooszination.cards.controller.App", {

        onInit: function () {
            this.getView().addStyleClass(
                this.getOwnerComponent().getContentDensityClass()
            );
        }

    });
});
