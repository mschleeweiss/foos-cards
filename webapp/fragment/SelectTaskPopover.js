sap.ui.define([
    "../controller/BaseController",
    "../util/Formatter",
    "sap/ui/core/Fragment",
    "sap/ui/core/syncStyleClass"
], function (BaseController, Formatter, Fragment, syncStyleClass) {

    return BaseController.extend("de.fooszination.cards.fragment.SelectTaskPopover", {
        constructor: function (oController) {
            this._oController = oController;
            this._oView = oController.getView();
        },

        exit: function () {
            delete this._oView;
        },

        openBy: function (oSource) {
            return new Promise((resolve) => {
                this._resolve = resolve;
                this._loadFragment().then(function (oFragment) {
                    oFragment.openBy(oSource);
                });
            });
        },

        /**
         * Returns a promise with the dialog
         */
        _loadFragment: function () {

            if (this._oFragment) {
                return Promise.resolve(this._oFragment);
            }

            const oView = this._oView;
            const oController = this._oController;

            const oFragmentController = {
                formatCategoryText: function () {
                    return oController.formatter.formatCategoryText.apply(oController, arguments);
                },
                formatCategoryIcon: function () {
                    return oController.formatter.formatCardIcon.apply(oController, arguments);
                },
                onPressClose: () => {
                    this._oFragment.close();
                    this._resolve();
                },
                onPressItem: (oEvent) => {
                    this._oFragment.close();
                    this._resolve(oEvent.getSource().getBindingContext().getObject());
                }
            };

            return Fragment.load({
                id: oView.getId(),
                name: this.getMetadata().getName(),
                controller: oFragmentController
            }).then((oFragment) => {
                this._oFragment = oFragment;
                oView.addDependent(oFragment);
                syncStyleClass(
                    oController.getOwnerComponent().getContentDensityClass(),
                    oView,
                    oFragment
                );
                return oFragment;
            });
        }
    });
}
);
