sap.ui.define([
    "sap/ui/core/UIComponent",
    "sap/ui/Device",
    "./model/ModelFactory"
], function (UIComponent, Device, ModelFactory, ErrorHandler) {

    return UIComponent.extend("de.fooszination.cards.Component", {
        metadata: {
            manifest: "json"
        },

        /**
         * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
         * @public
         * @override
         */
        init: function () {
            UIComponent.prototype.init.apply(this, arguments);

            this.setModel(ModelFactory.createDeviceModel(), "device");
            this.getRouter().initialize();
        },

        destroy: function () {
            this._oErrorHandler.destroy();
            UIComponent.prototype.destroy.apply(this, arguments);
        },

        /**
         * This method can be called to determine whether the sapUiSizeCompact
         * or sapUiSizeCozy design mode class should be set, which influences
         * the size appearance of some controls.
         *
         * @public
         * @return {string} css class, either 'sapUiSizeCompact' or
         *         'sapUiSizeCozy' - or an empty string if no css class should
         *         be set
         */
        getContentDensityClass: function () {
            if (!this._sContentDensityClass) {
                if (!Device.support.touch) {
                    this._sContentDensityClass = "sapUiSizeCompact";
                } else {
                    this._sContentDensityClass = "sapUiSizeCozy";
                }
            }
            return this._sContentDensityClass;
        }
    });
}
);
